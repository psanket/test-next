FROM node:16
WORKDIR /Users/sp/Nest/project-name
COPY . .
RUN yarn install 
RUN yarn run build
CMD [ "yarn", "start" ]