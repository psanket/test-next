import { Module } from '@nestjs/common';
import { Pool } from 'pg';
import { PG_CONNECTION } from '../constants';
import { ConfigModule } from '@nestjs/config';

const dbProvider = {
	provide: PG_CONNECTION,
	useFactory: () => {
		return new Pool({
			user: process.env.DB_USER,
			host: process.env.DB_HOST,
			database: process.env.DB_NAME,
			password: process.env.DB_PASS,
			port: process.env.DB_PORT
		})
	}
};

@Module({
	imports: [ConfigModule.forRoot({ ignoreEnvFile : true })],
	providers: [dbProvider],
	exports: [dbProvider]
})
export class DbModule {}
