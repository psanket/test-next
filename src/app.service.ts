import { Injectable, Inject } from '@nestjs/common';
import { PG_CONNECTION } from './constants';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {

  constructor (@Inject(PG_CONNECTION) private pool: any, private configService: ConfigService) {}

  async getRoles() {
    let rs = await this.pool.query("SELECT data FROM g.infomaster WHERE enabled = 1 AND type = 'Role'").catch((e) => { console.log('gui1',e); });
    if(!rs){ return { status: 'Fail' } };
    return rs.rows[0];
  }

  getHello(): string {
    return 'Hello World!';
  }
}
